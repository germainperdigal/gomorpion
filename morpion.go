package main
import "github.com/tadvi/winc"

const (
    taillegrille = 9
)

var nbCoups int = 0;

var (
    grille = [taillegrille]string{
        " ", " ", " ",
        " ", " ", " ",
		" ", " ", " "}
	)

var	infos string = "Au tour du joueur X"
var joueur string = "X"
var enjeu bool = true;

func main() {
	mainWindow := winc.NewForm(nil)
	mainWindow.Show()
	mainWindow.SetSize(600, 300)
	mainWindow.SetText("Morpion")

	btn := winc.NewPushButton(mainWindow) // Bouton pour close la fenêtre
	btn.SetText("Fermer")
	btn.SetPos(0, 0)
	btn.SetSize(100, 40)
	btn.OnClick().Bind(wndOnClose)

	afficherGrille(mainWindow)

	mainWindow.Center()
	mainWindow.Show()
	winc.RunMainLoop()
}

/* Affichage des boutons */
func afficherGrille(window *winc.Form) {
	mainWindow := window
	btn1 := winc.NewPushButton(mainWindow)
	btn1.SetText(grille[0]);
	btn1.SetPos(100, 60)
	btn1.SetSize(100, 40)
	btn1.OnClick().Bind(func(e *winc.Event) {
		jouer(0, mainWindow)
	})

	btn2 := winc.NewPushButton(mainWindow)
	btn2.SetText(grille[1]);
	btn2.SetPos(200, 60)
	btn2.SetSize(100, 40)
	btn2.OnClick().Bind(func(e *winc.Event) {
		jouer(1, mainWindow)
	})

	btn3 := winc.NewPushButton(mainWindow)
	btn3.SetText(grille[2]);
	btn3.SetPos(300, 60)
	btn3.SetSize(100, 40)
	btn3.OnClick().Bind(func(e *winc.Event) {
		jouer(2, mainWindow)
	})

	btn4 := winc.NewPushButton(mainWindow)
	btn4.SetText(grille[3]);
	btn4.SetPos(100, 100)
	btn4.SetSize(100, 40)
	btn4.OnClick().Bind(func(e *winc.Event) {
		jouer(3, mainWindow)
	})

	btn5 := winc.NewPushButton(mainWindow)
	btn5.SetText(grille[4]);
	btn5.SetPos(200, 100)
	btn5.SetSize(100, 40)
	btn5.OnClick().Bind(func(e *winc.Event) {
		jouer(4, mainWindow)
	})

	btn6 := winc.NewPushButton(mainWindow)
	btn6.SetText(grille[5]);
	btn6.SetPos(300, 100)
	btn6.SetSize(100, 40)
	btn6.OnClick().Bind(func(e *winc.Event) {
		jouer(5, mainWindow)
	})

	btn7 := winc.NewPushButton(mainWindow)
	btn7.SetText(grille[6]);
	btn7.SetPos(100, 140)
	btn7.SetSize(100, 40)
	btn7.OnClick().Bind(func(e *winc.Event) {
		jouer(6, mainWindow)
	})

	btn8 := winc.NewPushButton(mainWindow)
	btn8.SetText(grille[7]);
	btn8.SetPos(200, 140)
	btn8.SetSize(100, 40)
	btn8.OnClick().Bind(func(e *winc.Event) {
		jouer(7, mainWindow)
	})

	btn9 := winc.NewPushButton(mainWindow)
	btn9.SetText(grille[8]);
	btn9.SetPos(300, 140)
	btn9.SetSize(100, 40)
	btn9.OnClick().Bind(func(e *winc.Event) {
		jouer(8, mainWindow)
	})

	btnInfos := winc.NewPushButton(mainWindow)
	btnInfos.SetText(infos);
	btnInfos.SetPos(100, 190)
	btnInfos.SetSize(300, 40)
}

/* Assigne et affiche le joueur */
func jouer(position int, window *winc.Form) {
	if enjeu {
		if(grille[position] == " ") {
			if joueur == "X" {
				grille[position] = joueur
				joueur = "O"
				nbCoups++;
				infos = "Au tour du joueur O"
			} else {
				grille[position] = joueur
				joueur = "X"
				nbCoups++;
				infos = "Au tour du joueur X"
			}
		} else {
			infos = "Case déjà jouée, joueur "+joueur
		}
	if(verifier() != " ") {
		infos = "Victoire ? "+verifier();
		enjeu = false;
	} else {
		if(nbCoups == 9) {
			infos = "Egalité !"
			enjeu = false;
		}
	}
	} else {
		infos = "Partie terminée !"
	}
	afficherGrille(window)	
}

func wndOnClose(arg *winc.Event) {
	winc.Exit()
}

/* Vérifie et retourne un bool si victoire */
func verifier() string {
	// Vérification horizontale //
	for i := 0; i < 3; i++ {
		if grille[i*3] == grille[(i*3)+1] && grille[(i*3)+1] == grille[(i*3)+2] {
			return grille[i*3];
		}
	}
	// Vérification verticale //
	for i := 0; i < 3; i++ {
		if grille[i] == grille[(i+3)] && grille[(i+3)] == grille[(i+6)] {
			return grille[i];
		}
	}
	// Vérification diago //
	if grille[0] == grille[4] && grille[4] == grille[8] {
		return grille[0];
	} 
	if grille[2] == grille[4] && grille[4] == grille[6] {
		return grille[2];
	} 

	// Si pas de combinaison ou tableau vide
	return " "
}